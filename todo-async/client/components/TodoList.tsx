import * as React from 'react';
const { Component } = React;

import TodoComponent from './Todo';
import { Todo } from '../models/todos';

export interface ITodoListProps {
  todos: Todo[],
  onTodoClick: (Todo) => any
}

class TodoList extends Component<ITodoListProps, {}> {
  render() {
    return (
      <ul>
        {this.props.todos.map(todo =>
          <TodoComponent
            key={todo.id}
            {...todo}
            onClick={() => this.props.onTodoClick(todo)} />
        )}
      </ul>
    )
  }
}

export default TodoList;
