import * as React from 'react';
import * as ReactDOM from 'react-dom';
const { Component } = React;

import { Todo } from '../models/todos';

export interface IAddTodoProps {
  onAddClick: (Todo) => any
}

class AddTodo extends Component<IAddTodoProps, {}> {
  refs: {
    [string: string]: any;
    input: any;
  }

  render() {
    return (
      <div>
        <input type='text' ref='input' />
        <button onClick={e => this.handleClick(e)}>
          Add
        </button>
      </div>
    );
  }

  handleClick(e) {
    const node = this.refs.input;
    const text = node.value.trim();
    this.props.onAddClick(text);
    node.value = '';
  }
}

export default AddTodo;
