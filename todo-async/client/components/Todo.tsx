import * as React from 'react';
const { Component } = React;

export interface ITodoProps {
  onClick: () => any
  completed: boolean,
  text: string,
  key: any
}

class Todo extends Component<ITodoProps, {}> {
  render() {
    return (
      <li
        onClick={this.props.onClick}
        style={{
          textDecoration: this.props.completed ? 'line-through' : 'none',
          cursor: this.props.completed ? 'default' : 'pointer'
        }}>
        {this.props.text}
      </li>
    );
  }
}

export default Todo;
