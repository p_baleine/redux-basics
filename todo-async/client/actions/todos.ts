import { createAction, Action } from 'redux-actions';
import { Todo } from '../models/todos';
import * as types from '../constants/ActionTypes';

let nextTodoId = 1;

const addTodo = createAction<Todo>(
  types.ADD_TODO,
  (text: string) => ({ id: nextTodoId++, text, completed: false })
);

const completeTodo = createAction<Todo>(
  types.COMPLETE_TODO,
  (todo: Todo) => todo
);

const setVisibilityFilter = createAction<string>(
  types.SET_VISIBILITY_FILTER,
  (filter: string) => filter
);

export {
  addTodo,
  completeTodo,
  setVisibilityFilter
};
