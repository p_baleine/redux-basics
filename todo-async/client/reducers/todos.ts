import { combineReducers } from 'redux';
import { handleActions, Action } from 'redux-actions';

import { SHOW_ALL } from '../constants/VisibilityFilters';
import { Todo } from '../models/todos';
import {
  ADD_TODO, COMPLETE_TODO, SET_VISIBILITY_FILTER
} from '../constants/ActionTypes';

const initialState = [<Todo>{
  text: "Hello, world!",
  completed: false,
  id: 0
}];

const todos = handleActions<Todo[]>({
  [ADD_TODO]: (state: Todo[], action: Action): Todo[] => {
    return [...state, action.payload];
  },

  [COMPLETE_TODO]: (state: Todo[], action: Action): Todo[] => {
    return state.map(todo =>
      todo.id === action.payload.id ?
        Object.assign({}, todo, { completed: !todo.completed }) :
        todo
    );
  }
}, initialState);

const visibilityFilter = handleActions<string>({
  [SET_VISIBILITY_FILTER]: (state: string, action: Action): string => {
    return action.payload;
  }
}, SHOW_ALL);

export default combineReducers({
  visibilityFilter,
  todos
});
